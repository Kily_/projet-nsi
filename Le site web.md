# FONCTIONNEMENT DES SITES WEB
Pour consulter un site web, vous lancez un programme appelé le navigateur web.
Ces sites fonctionne avec le **HTML** et le **CSS**, deux languages informatique. 
Le site est donc rediger dans ces deux langues et l'ordinateur se base sur ses connaissance 
pour ensuite afficher ce que l'on voit en ouvrant un site. 

Cependant on peux voir un site dans sa version "brute" en effectuant `cntrl` + `U` 
ou en faisant clique droit puis éxaminer l'élément. 


## HTML et CSS : DEUX LANGUAGES POUR CRÉER UN SITE WEB
Pour créer un site web, Il faut donner des instructions à l'ordinateur et non
simplement taper le texte qui devra figurer dans le site. Il faut également 
indiquer où placer ce texte, insérer des images, etc.  

Pour expliquer à l'ordinateur ce qu´on veux faire, il va falloir utiliser 
un langage qu'il comprend.  
Pour cela on en utilise deux :
- le [HTML](https://gitlab.com/Kily_/projet-nsi/-/blob/master/HTML.md), 
qui nous servira à structurer et donner du sens au contenu.   
- le [CSS](https://gitlab.com/Kily_/projet-nsi/-/blob/master/CSS.md), qui nous servira à mettre en forme le contenu.

⚠️ **Attention**, il ne faut pas utiliser le HTML pour mettre en forme du contenu
c'est à dire utiliser le HTML pour faire le travail du CSS. 
## DIFFÉRENTES FAÇONS DE TRAVAILLER
Lorsque l'on code, on a la possibilité de travailler :
- En local. C'est à dire travailler avec des fichiers enregistrer sur notre propre ordinateur.
Nous seul pouvons voir ce que l'on fait. Cette manière de travailler comporte des 
avantages comme le fait de pouvoir créer ou tester de nouvelles fonctionalitées 
sans impacter le fonctionnement d'un site live.

- En production. C'est à dire travailler en production signifie intervenir sur des fichiers déjà stockés sur un
serveur web. Si l'on veut effectuer des modification cela impactera le site en live.

Ainsi, si on possède déjà un site et que l'on veux effectuer des modifications
mieux vaut travailler en local et non en production. En effet, si on modifie le 
code en live, le site pourrait ne pas ou mal fonctionner pendant un certain temps
et déranger les internautes. Il 
## ÉDITEUR DE TEXTE
Les éditeur de texte sont des programmes dédiés à l'écriture de code. On peut en 
général les utiliser pour de multiples langages, pas seulement HTML et CSS. 
Il en existe une multitude disponible sur internet, gratuit ou bien payant, fonctionnant
avec des environements différents. 

**Sous Windows**  
Voici quelques logiciels sous Windows 
* Sublime Text 
* Notepad++ 
* Brackets 
* jEdit 
* PSpad 
* ConTEXT 

Voici quelques **logiciels multi-plateformes** :

* Sublime Text 
* Brackets 
* jEdit 
* Smultron 
* TextWrangler

**Sous Linux**  
Les éditeurs de texte sont légion sous Linux. Certains d'entre eux sont installés 
par défaut, d'autres peuvent être téléchargés facilement via le centre de téléchargement 
(sous Ubuntu ^^ notamment) ou au moyen de commandes.

* Sublime Text 
* Brackets 
* gEdit 
* Kate 
* vim 
* Emacs
* jEdit

## NAVIGATEUR  
Le navigateur est le programme qui nous permet de voir les sites web. Il lit le 
code HTML et CSS pour afficher un résultat visuel à l'écran. Cependant, les différents 
navigateurs n'affichent pas le même site exactement de la même façon. Il faudra 
donc vérifier régulièrement que notre site fonctionne correctement sur la plupart 
des navigateurs.

Les principaux navigateurs sont :
* Google Chrome
* Mozilla Firefox
* Internet Explorer 
* Edge
* Safari
* Opera

Les navigateurs n'affichent pas toujours un même site web exactement de la même 
façon car ils ne connaissent pas toujours les dernières fonctionnalités de HTML 
et CSS. Cependant, aujourd'hui ils supportent un grand nombre de fonctionnalités
grâce aux mises à jour très fréquentes. Le site [caniuse](https://caniuse.com) tient à jour une liste 
des fonctionnalités prises en charge par les différentes versions de chaque navigateur.

Il existe également des différences entre les navigateurs mobile. Il est conseillé 
de tester son site sur ces appareils aussi. En particulier, l'écran étant beaucoup 
moins large, il faudra vérifier que notre site s'affiche correctement.