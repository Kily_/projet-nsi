# CSS
Le **CSS** est le dimunitif de **"Cascading StyleSheets"** que l'on pourrait traduire
par **"Feuille de Style en Cascade"**. Il a été crée en 1996 et a pour rôle de mettre 
en forme du contenu en lui appliquant des **"styles"**. Le CSS permet, par exemple,
de définir la taille, la couleure ou bien l'alignement d'un texte.