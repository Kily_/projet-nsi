# HTML  
Le **HTML** est l'abréviation de **« HyperText Markup Language »** qu'on peut traduire par **« langage 
de balises pour l'hypertexte »**. Ce language a été crée 1991 par Tim Berners-Lee et a pour fonction
de structurer et de donner du sens à un contenu. Grâce au html, on peux par exemple
indiquer à un navigateur qu'un texte doit être considéré comme un paragraphe ou bien
comme un titre. 
## Élements, balises et attributs  
Le language html tout entier va reposer sur l'utilisation **d'éléments**. Il est donc 
nécessaire de comprendre d'abord ce qu'est un élément pour comprendre le html.  

**LES ÉLÉMENTS**  
Ils permettent de structurer le contenu et différencier chaque type de contenu, 
par exemple définir un titre comme ci-dessous. Il se compose d'un texte entouré
de deux balises ou alors d'une balise seule.
```html
<title>Ceci est un titre<title/>
```

**LES BALISES**  
Elles sont entourées de « chevrons », c'est-à-dire des symboles `<` et `>`.
Elles indiquent la nature du texte qu'elles encadrent. Par exemple : Pour dire 
"*Ceci est le titre de la page*" on utilise la balise `<title>`

On distingue deux types de balises : les balises en **paires** et les balises **orphelines**.  

   a) **Les balises en paires**  
   On distingue une **balise ouvrante** `<title>` et une **balise fermante** `</title>` 
qui indique que le titre se termine. Cela signifie pour l'ordinateur que tout ce 
qui n'est pas entre ces deux balises n'est pas un titre.
```html
<title>Ceci est un titre<title/>
```
b) **Les balises orphelines**  
Ce sont des balises qui servent le plus souvent à insérer un élément à un endroit précis.
Par exemple, pour une image il n'est pas nécessaire de délimiter son début et sa 
fin, on veut juste dire à l'ordinateur « Insère une image ici ».
```html
<image/>
```
**LES ATTRIBUTS**  
Ce sont "les options" des balises. Ils viennent les compléter en les définissant 
plus précisément ou en amènant des informations supplémentaires.  
L'attribut se place après le nom de la balise ouvrante et a le plus souvent une valeur.
```html
<balise attribut="valeur">
```
## Structure minimale
Pour qu'une page soit "valide", elle doit obligatoirement comporter certains éléments
et suivre un shéma précis.
```html
<!DOCTYPE html>
<html>
     <head>
         <title>Titre de la page</title>
         <meta charset = "utf-8">
     </head>
     <body>
     
     </body>
</html>
```
Le `<!DOCTYPE>` sert à préciser le type de document. On rajoute ensuite le language
utilisé, en outre, le html.  
Ensuite, on inclu un élément html avec les balisent `<html>` et `</html>`. Il représente 
toute la page web, on insert donc tout le contenu de notre page dans cet élément. 

Puis, on inclu deux éléments obligatoirs au même niveau :  
- Le premier est symbolisé par les balises `<head>` et `</head>`. Il contiens des informations 
générales dont la page va avoir besoin pour fonctionner, notamment le titre et 
le type d'encodage de la page. Ces informations ne sont donc pas visible.

- Le second est symbolisé par les balises `<body>` et `</body>`. Il contiens les 
informations visibles de la page : texte, image, etc.

Dans l'élément head on indique donc deux autres éléments : 
- Le premier est symbolisé par les balises `<title>` et `</title>`. Il nous sert 
à indiquer le titre de la page. Il sera visible dans le navigateur.
- Le second est un élément de type "méta" et nous permet de definir le type d'encodage
de la page. Il est symbolisé par une balise orpheline `<meta>`. Cepandant on précise
un attribut `charset` en lui attribuant la valeur de l'encodage, ici `utf-8`

## Indentation et commentaire

## Retour à la ligne et espaces 
## Listes