Tout d'abord avant de créer un [site web](https://gitlab.com/Kily_/projet-nsi/-/blob/master/Le%20site%20web.md),
il faut connaître les languages [HTML](https://gitlab.com/Kily_/projet-nsi/-/blob/master/HTML.md) 
et [CSS](https://gitlab.com/Kily_/projet-nsi/-/blob/master/CSS.md). Je vous invite
donc à cliquer sur ces derniers afin de connaître quelques bases.

## DIFFÉRENTES FAÇONS DE TRAVAILLER
Lorsque l'on code, on a la possibilité de travailler :
- En local. C'est à dire travailler avec des fichiers enregistrer sur notre propre ordinateur.
Nous seul pouvons voir ce que l'on fait. Cette manière de travailler comporte des 
avantages comme le fait de pouvoir créer ou tester de nouvelles fonctionalitées 
sans impacter le fonctionnement d'un site live.

- En production. C'est à dire travailler en production signifie intervenir sur des fichiers déjà stockés sur un
serveur web. Si l'on veut effectuer des modification cela impactera le site en live.

Ainsi, si on possède déjà un site et que l'on veux effectuer des modifications
mieux vaut travailler en local et non en production. En effet, si on modifie le 
code en live, le site pourrait ne pas ou mal fonctionner pendant un certain temps
et déranger les internautes. Il faut donc copier le site et le modifier en local.