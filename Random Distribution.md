# Random Distribution *(inspiré de François Morellet)*





## François Morellet
![François Morellet Photo](https://gw.geneanet.org/public/img/media/deposits/7d/0d/5508326/medium.jpg?t=1463044728)

**François Charles Alexis Albert Morellet** naît à Cholet le 30 avril 1926 et mort
le 10 mai 2016 dans cette même ville. C'est un artiste français : Peintre, 
graveur et sculpteur. il est considéré comme l'un des acteurs majeurs de **l'abstraction géométrique** 
de la seconde moitié du xxe siècle et un précurseur du **minimalisme**. 

François Morellet dès le début des **années 1950** a fondé son art en utilisant des 
**formes élémentaires**, la ligne, la grille, le carré, arrangées selon des principes 
simples, débouchant ainsi sur la production de **structures illimitées** et de 
**rythmes répétitifs**, neutres tant dans leur forme que dans leur exécution.  

À la fin des années 1950, il voyage au Brésil où il se lie d’amitié avec **Almir da Silva Mavignier**. 
Il y découvre l’oeuvre de Max Bill et l’art concret, ce qui sera fondamental dans 
son orientation artistique. Dès son retour en France, au début de l’année 1951, 
il réalise ses premières **œuvres géométriques**. Ce manifeste alors son appartenance 
à **l'abstraction géométrique**

## 40 000 carrés
![40 000 carrés](http://www.artnet.fr/WebServices/images/ll00114lldJEjGFgo4qCfDrCWvaHBOc3PtC/françois-morellet-40.000-carrés-(set-of-8).jpg)  
Les **40 000 carrés** est une oeuvre de François Morellet réalisé en 1963. Il parvient 
à l’abstraction à travers cette peinture **dépouillée** à la facture volontairement 
**neutre** et **privée de toute sensualité**.  

Cependant, son humour va subvertir à cette œuvre a priori froide. En effet, sa 
réalisation s'appuie sur un principe assez loufoque et intéressant : l'oeuvre 
comporte réellement **40 000 éléments** rouges et bleus mais disposés aléatoirement 
en fonction des chiffres **pairs** et **impairs** d'un **annuaire téléphonique** ! 
Il y a en tout **50% d'éléments rouges** et **50% d'éléments bleus**. Il en existe 
en  differents coloris dans plusieurs villes differentes. (*40 000 carrés verts 
et rouges, gris et noirs, etc*).

Le principe fondé sur le **système binaire** est le résultat anticipant la **révolution numérique** 
et bouleversant les **conventions artistiques**.

## Explication du programme

Le programme vise à reproduire un l'oeuvre à quelques différences près. 

- Importation

```python
# On importe la bibliothèque tkinter
from tkinter import *
# On importe la fonction randrange de la bibliothèque random
from random import randrange
```
Ici on travail sur un interfarce graphique en important `tkinter`  
La fonction `randrange()` correspond à un tirage au hasard dans la liste d'entiers 
qui serait générée par la fonction `range` avec les mêmes paramètres.  
Par exemple *range(6)* génère la liste [0,1,2,3,4,5] et *randrange(6)* tire un 
élément au hasard dans la liste [0,1,2,3,4,5].  
**Attention !** Les méthodes randrange() et randint() ont un paramétrage différent : 
randrange(9) = randint(0,8)

- Inisialiser
```python
# On détermine les dimenssions du repère et d'une case
haut = 45  # abscisse du repère orthogonal
larg = 90  # ordonée du repère orthogonal
cote = 15  # côté d'une case
```
les 40 000 carrés sont sous forme de **repère orthogonal**. On initialise les 
paramètres de ce repère. On initialise également le côté d'une case.

```python
rouge = 0
bleu = 1
nb_rouges = haut*larg  # tout le monde est rouge
nb_bleus = 0  # il n'y a pas de cases bleus 
```
On initialise les **variables**. Au départ tout les carrés sont **rouge** et pour 
être fidèle à l'oeuvre, on rajoutera des carrés bleus jusqu'à ce qu'il y ai **autant de
carrés rouges que de carrés bleus**.

- Créer les matrices 
```python
case =[[0 for row in range(haut)]for col in range(larg)]
rempli =[[rouge for row in range(haut)]for col in range(larg)]
```
Deux matrices seront utilisées : `case` mémorisera les **cases**, `rempli` mémorisera 
la **couleur des cases**. La matrice est un tableau comportant des **lignes** (row)
et des **colonnes** (col).

- Les fonctions 
```python
# Dessiner toutes les cases 
def dessiner ():
    for y in range(haut):
        for x in range(larg):
            if rempli[x][y]==rouge:
               color = "red"
            else:
               color = "blue"
            canvas.itemconfig(case[x][y], fill = color)
```
Dans cette fonction, *y* représente les ordonnées et *x* représente les abscisses. On utilise la matrice
`rempli` avec la condition `if` qui nous permet de "dessiner" en rouge si la couleur
de la cellule est rouge ou si ce n'est pas le cas, en bleu. Puis on utilise la 
méthode `itemconfig` qui nous permet de changer la couleur de la case. 
```python
def init():
    global nb_rouges, nb_bleus
    for y in range(haut):
        for x in range(larg):
            rempli[x][y] = rouge # tout le monde est rouge au départ
            case[x][y] = canvas.create_rectangle((x*cote, y*cote, (x+1)*cote,
                         (y+1)*cote), outline="gray", fill="red")
```

Les variables nb_rouges et nb_bleus sont **globales**. Cela signifie qu'elles ont 
été déclarées dans le programme principal, donc hors de la procédure. On reprend 
la matrice `rempli` en indiquant que toutes les cases sont rouges. Puis on prend 
la matrice `case` pour créer les cases. On utilise donc `canvas` qui est une 
zone destinée à contenir des dessins ou d'autres figures (*texte, widgets,frame,etc*). 
On rajoute la méthode `create_rectangle` pour créer un rectangle. `outline` nous 
permet de mettre les contours en gris et `fill` de remplir la case en rouge.

```python
# place au hasard 50% de cellules bleus
while nb_bleus<nb_rouges:
    x = randrange(larg)
    y = randrange(haut)
    if rempli[x][y] == rouge:
       rempli[x][y]  = bleu
       nb_bleus += 1
       nb_rouges -= 1
dessiner ()
```
Pour être fidèle à l'oeuvre, les cases bleus seront **placées aléatoirement**. 
Pour cela, on utilise la fonction `randrange()` déjà présenter auparavant, qui choisira
une case au hasard. **Tant que** le nombre de cases bleus **est inférieur** à celui 
des cases rouges, **si** la case est rouge elle deviendra bleu. Dans ce cas on 
rajoute 1 a nb_bleus et on enlève 1 à nombre_rouges.  

```python
#   Déterminer la couleur des voisins
def couleur_voisin(a,b):
    voisin = randrange(8)
    if voisin==1:
        couleur = rempli[(a-1)%larg][(b+1)%haut]
    elif voisin==2:
        couleur = rempli[a][(b+1)%haut]
    elif voisin==3:
        couleur = rempli[(a+1)%larg][(b+1)%haut]
    elif voisin==4:
        couleur = rempli[(a-1)%larg][b]
    elif voisin==5:
        couleur = rempli[(a+1)%larg][b]
    elif voisin==6:
        couleur = rempli[(a-1)%larg][(b-1)%haut]
    elif voisin==7:
        couleur = rempli[a][(b-1)%haut]
    else:
        couleur = rempli[(a+1)%larg][(b-1)%haut]
    return couleur
```
Pour innover, je rajoute une règle inspirer du [jeu de la vie](https://gitlab.com/Kily_/le-jeu-de-la-vie/-/blob/master/Jeu%20de%20la%20vie.md) où les cases
ont des influence sur leur voisins. Avec cette nouvelle règle, une case choisira
un voisin et prendra sa couleur. Ici on détermine la couleur des cases 
voisines. On utilise la fonction `randrange` qui va choisir un voisin parmis les 8
voisins possibles. 
```python
# Appliquer la règle
def calculer():
    global nb_rouges, nb_bleus
    x = randrange(larg)
    y = randrange(haut)
    nouvelle_couleur = couleur_voisin(x,y)
    if rempli[x][y] != nouvelle_couleur:
        if nouvelle_couleur == rouge:
            nb_rouges += 1
            nb_bleus -= 1
        else :
            nb_rouges -= 1
            nb_bleus += 1
        rempli[x][y] = nouvelle_couleur
        if rempli[x][y] == rouge:
            color = "red"
        else:
            color = "blue"
        canvas.itemconfig(case[x][y], fill=color)
```
Ici, on réutilise les fonctions globales nb_rouges et nb_bleus. La couleur du voisin
sera la **nouvelle couleure** de la case. On utilise deux conditions :  
-- Si la couleur de la case est différente de la couleur de son voisin  
-- Si la couleur du voisin est rouge  
Alors, la case sera rouge donc on rajoute 1 à nb_rouges et on enlève 1 à nb_bleus. 
Autrement, on enlève 1 à nb_rouges et on rajoute 1 à nb_bleus. Puis on attribue
une couleur à la case en utilisant la variable `color`.  
```python
# Calculer et dessiner le prochain tableau
def tableau():
    calculer()
    fenetre.after(250, tableau)
```
Dans cette dernière fonction, on utilise la méthode `after` qui déclenche l'appel 
d'une fonction après qu'un certain laps de temps se soit écoulé. Ici, après 250 ms
la méthode appelle la fonction `tableau`. Enfin la fonction `calculer` est invoqué.

- Affichage
```python
# Afficher
fenetre = Tk()
fenetre.title("Random distribution")
canvas = Canvas(fenetre, width=cote*larg, height=cote*haut, highlightthickness=0)
canvas.pack()
init()
tableau()
fenetre.mainloop()
```
Cela nous permet d'afficher la fenêtre, le titre, le canevas et les fonctions.

## La vidéo 
Parce que c'est toujours intéressant d'avoir plusieurs formats différents, voici
le lien de la vidéo réalisé sur ce projet : https://youtu.be/w8sU44R132k

**PROGRAMMEZ BIEN !!!**  

Lien vers le programme en entier : https://lyceend-my.sharepoint.com/:u:/g/personal/kmajani_notre-dame-reze_fr/EVKnkQQPRQRLrn15jGJA1WQBC8hnDHc9PzAwkXFS-vfPfw?e=anblRK

# Sources 
- Biographie  
https://slash-paris.com/fr/artistes/francois-morellet/a-propos  
https://francoismorellet.wordpress.com/biographie-et-influences/  
https://fr.wikipedia.org/wiki/François_Morellet  

- 40 000 carrés  
https://www.musees.strasbourg.eu/-uvre-mamcs/-/entity/id/220144?_eu_strasbourg_portlet_entity_detail_EntityDetailPortlet_returnURL=https%3A%2F%2Fwww.musees.strasbourg.eu%2F-uvre-mamcs%2F-%2Fentity%2Fid%2F220148%3F_eu_strasbourg_portlet_entity_detail_EntityDetailPortlet_returnURL%3Dhttps%253A%252F%252Fwww.musees.strasbourg.eu%252Fcollection-mamcs%253Fp_p_id%253Dcom_liferay_asset_publisher_web_portlet_AssetPublisherPortlet_INSTANCE_j6r4Oo2KyzWz%2526p_p_lifecycle%253D0%2526p_p_state%253Dnormal%2526p_p_mode%253Dview%2526p_p_col_id%253Dcolumn-1%2526p_p_col_pos%253D2%2526p_p_col_count%253D3%2526p_p_auth%253DqIUhFTyi  
https://slash-paris.com/fr/artistes/francois-morellet/a-propos